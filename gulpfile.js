const loopTasks = (tasks, cbFn) => {
    return Object.getOwnPropertyNames(tasks)
        .reduce((container, taskName) => {
            const task = tasks[taskName];
            return cbFn(task);
        }, null);
};
const Config = require('./config.js');
const taskNames = Object.getOwnPropertyNames;
const gulp = require('gulp');

const $ = {};
// Html
$.fileIncl = require('gulp-file-include');
$.btfy = require('gulp-jsbeautifier');
$.pug = require('gulp-pug');

// CSS
$.sass = require('gulp-sass');
$.autoprefixer = require('gulp-autoprefixer');
$.sourcemaps = require('gulp-sourcemaps');

// JavaScript
$.concat = require('gulp-concat');
$.uglify = require('gulp-uglify');

// sprite generation
$.spritesmith = require('gulp.spritesmith');

// Angular
$.htmlmin = require('gulp-htmlmin');
$.ngTemplateCache = require('gulp-angular-templatecache');
$.karma = require('karma').Server;                     // testing tool for Angularjs

/* Util */
// Server/Testing
$.browserSync = require('browser-sync').create();

// Version
$.bump = require('gulp-bump');

// General
$.if = require('gulp-if');                             // e.g. $.if(condition, fnToRun)
$.yargs = require('yargs').argv;
$.rename = require('gulp-rename');
$.clean = require('gulp-clean');
$.zip = require('gulp-zip');

// Log
$.taskListing = require('gulp-task-listing');
$.runTasks = require('run-sequence');
$.plumber = require('gulp-plumber');
$.print = require('gulp-print');


// Build All Pipeline
gulp.task('build', () => {
    $.runTasks(
        'clean',
        'html',
        'js',
        'css',
        'copy'
    );
});

gulp.task('serv', () => {
    // clean has to be separated from build
    // $.runTasks('clean', 'js', ...,  'watch') or $.runTasks('build', 'watch') does not work unfortunately due to plugin bug
    $.runTasks(
        'clean',
        'html',
        ['js', 'css'],      // parallel
        'copy',
        'watch'
    );
});

// Html
gulp.task('html', () => {
    const { tasks, defOption } = Config.html.pug;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.plumber() )
            .pipe( $.pug(defOption) )
            .pipe( gulp.dest(task.outputPath) );
    });
});

// Sass
gulp.task('css', () => {
    const isProd = $.yargs.prod;
    const autoprefixerOption = Config.css.autoprefixer.defOption;
    const { tasks, defOption } = Config.css.sass;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.plumber() )
            .pipe( $.if(!isProd, $.sourcemaps.init()) )
            .pipe( $.sass({outputStyle: isProd ? 'compressed' : 'nested'}) )
            .pipe( $.autoprefixer(autoprefixerOption) )
            .pipe( $.if(!isProd, $.sourcemaps.write()) )  // Use Sourcemap at LocalBuild
            .pipe( $.rename({ extname: isProd ? '.min.css' : '.css' }) )
            .pipe( gulp.dest(task.outputPath) );
    });
});

// JS
gulp.task('js', ['html-ng'], () => {
    const isProd = $.yargs.prod;
    const { defOption, tasks } = Config.js.uglify;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.plumber() )
            .pipe( $.concat(task.outputFile) )
            .pipe( $.if(isProd, $.uglify(defOption)) )  // Don't use Uglify at LocalBuild
            .pipe( gulp.dest(task.outputPath) );
    });
});
gulp.task('html-ng', () => {
    const { defOption, tasks } = Config.ng.optimizeHtml;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( $.pug() )
            .pipe( $.htmlmin(defOption.htmlmin) )
            .pipe( $.ngTemplateCache(task.outputFilename, defOption.templatecache) )
            .pipe( gulp.dest(task.outputPath) );
    });
});

// Unit Testing
gulp.task('karma', (done) => {
    const watch = $.yargs.watch;
    const { karmaOption } = Config.ng;

    karmaOption.singleRun = watch ? false : true;
    $.karma.start(karmaOption,  (karmaResult) => {
        done();
    });
});

// Files that need to be copied across
gulp.task('copy', () => {
    const { tasks } = Config.util.copy;

    return loopTasks(tasks, (task) => {
        return gulp.src(task.inputFiles)
            .pipe( gulp.dest(task.outputPath) );
    });
});


/* Utility */
// Task Listing
gulp.task('tasks', $.taskListing);

// Clean up
gulp.task('clean', () => {
    const { defOption, tasks } = Config.util.clean;

    return loopTasks(tasks, (task) => {
        return gulp.src( task.inputFiles, defOption )
            .pipe( $.plumber() )
            .pipe( $.print() )
            .pipe( $.clean() );
    });
});

// Monitor & Refresh Browser
gulp.task('watch', () => {
    const { defOption, watchFiles } = Config.util.browserSync;

    $.browserSync.init(defOption);
    gulp.watch(watchFiles.src.scss, ['css']);
    gulp.watch(watchFiles.src.js, ['js'] );
    gulp.watch(watchFiles.src.html, ['copy']);
    gulp.watch(watchFiles.dist).on('change', $.browserSync.reload);
});

// Verion Bumping
gulp.task('bump', () => {
    /**
     * The following are for Input in command line:
     * --type=pre                   // bump the prerelease version      *.*.*-x
     * --type=patch OR no flag      // bump the patch version           *.*.x
     * --type=minor                 // bump the minor version           *.x.*, e.g. 0.1.0 --> 0.2.0
     * --type=major                 // bump the major version           x.*.*
     * --version=1.2.3              // bump to a specific version & ignore other flags
     */
    var msg = 'Bumping version';
    var options = {};
    var version = $.yargs.version;
    var type = $.yargs.type;

    // If user provide a specific version in the cmd
    if (version) {
        options.version = version;
        msg += ' to ' + version;

    // If specific version is not provided
    } else {
        options.type = type;
        msg += ' for a ' + type;
    }

    // Bumping up the version in json files
    return gulp
        .src([ './package.json' ])
        .pipe($.bump(options))
        .pipe(gulp.dest('./'));
});


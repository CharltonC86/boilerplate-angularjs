/*
 *  Required Format for Addition/Modification
 *  ==========================================
 *  <groupName>: {
 *      <pluginName1>: {
 *          defOption: {        // <an object or string for the plugin setting>,
 *              <prop1>: val1
 *              <propN>: valN
 *          }
 *          tasks: {
 *              <taskName>: {
 *                  inputFiles:
 *                  outputPath:
 *                  outputFile:
 *                  overrideSetting:
 *              }
 *          }
 *      }
 *  }
 */


module.exports = {
    // HTML
    html: {
        pug: {
            defOption: {
                base: '.'
            },
            tasks: {
                main: {
                    inputFiles: [
                        'src/*.pug'
                    ],
                    outputPath: 'dist/'
                }
            }
        }
    },

    // CSS
    css: {
        autoprefixer: {
            defOption: {
                browsers: ['last 10 version', 'IE 9'],
                ifCascade: false
            }
        },
        sass: {
            tasks: {
                global: {
                    inputFiles: 'src/sass/main.scss',              // NOTE: wildcard "*" is not allowed
                    outputPath: 'dist/css/'
                },
            }
        }
    },

    // JS
    js: {
        uglify: {
            defOption: { mangle: true },
            tasks: {
                main: {
                    inputFiles: [
                        'src/js/lib/angular.js',
                        'src/js/lib/angular-route.js',
                        'src/js/main.js',
                        'src/js/ng-html.js',
                        'src/js/route/config.js',
                        'src/js/service/*.js',
                        '!src/js/service/*.spec.js',
                        'src/js/filter/*.js',
                        '!src/js/filter/*.spec.js',
                        'src/ui/**/**/*.js',
                        '!src/ui/**/**/*.spec.js'
                    ],
                    outputFile: 'main.min.js',
                    outputPath: 'dist/js/'
                }
            }
        },
    },

    // UTILITY
    util: {
        // Clean & Copy
        clean: {
            defOption: { read: false },
            tasks: {
                main: {
                    inputFiles: [
                        'dist/*',
                        'src/js/ng-html.js'
                    ]
                }
            }
        },
        copy: {
            tasks: {
                // html: {
                //     inputFiles: [ 'src/index.html', ],
                //     outputPath: 'dist/'
                // }
            }
        },

        // Server & Watch Files
        browserSync: {
            defOption: {
               server: { baseDir: './dist/' },
               startPath: '/#/',
               reloadDelay: 1500
            },
            watchFiles: {
                src: {
                    html: [
                        'src/*.pug',
                    ],
                    scss: [
                        'src/sass/**/*.scss',
                        'src/*.scss',
                        'src/ui/**/**/*.scss',
                    ],
                    js: [
                        'src/js/**/*.js',
                        'src/js/*.js',
                        'src/ui/**/**/*.js',
                        'src/ui/component/**/template.pug'
                    ]
                },
                dist: [
                    'dist/index.html',
                    'dist/css/*.css',
                    'dist/js/*.js',
                    'dist/js/*.min.js'
                ]
            }
        }
    },

    // ANGULARJS
    ng: {
        // Minify Html into a Js file & Place html Contnet in $templatecache
        optimizeHtml: {
            defOption: {
                htmlmin: { collapseWhitespace: true },
                templatecache: {
                    standAlone: false,      // if use a separate module name for the template
                    module: 'demo'          // specify the name of new module (if not `template` module)
                }
            },
            // Individual View/Component template file
            tasks: {
                main: {
                    inputFiles: [
                        'src/ui/**/**/template.pug',
                    ],
                    outputPath: 'src/js/',
                    outputFilename: 'ng-html.js'         // will be deleted after concat & minified
                }
            }
        },

        // Unit Testing - Karma Option
        karmaOption: {
            // BROWSER - BROWSER & URL ETC
            browsers: [ 'Chrome' ],
            port: 8090,
            singleRun: true,

            // FILES TO BE LOADED/EXCLUDED IN THE BROWSER (SHOULD BE IN ORDER)
            autoWatch: true,                // Watch for file changes to automatically rerun the test
            basePath: './',                 // relative to 'karma.conf.js'
            files: [
                // Libraries
                'src/js/lib/angular.js',
                'src/js/lib/angular-route.js',
                'src/js/lib/angular-mocks.js',

                // Any External templateUrl file (ONLY IF not inline)
                // 'dist/view/templateUrl.html',

                // Js & Spec files
                'src/js/main.js',
                'src/js/ng-html.js',
                'src/js/route/config.js',
                'src/js/service/*.js',
                '!src/js/service/*.spec.js',
                'src/js/filter/*.js',
                '!src/js/filter/*.spec.js',
                'src/ui/**/**/*.js',
                '!src/ui/**/**/*.spec.js'
            ],
            exclude: [],

            // PLUGINS INSTALLED/USED FOR TESTING & GENERATING REPORTS
            plugins: [
                'karma-chrome-launcher',
                'karma-jasmine',
                'karma-spec-reporter',
                'karma-coverage',
                'karma-ng-html2js-preprocessor'
            ],

            // MAIN TESTING FRAMEWORK USED FOR TESTING
            frameworks: [
                'jasmine'          // matches npm pkg 'karma-jasmine'
            ],

            // OVERVIEW OF FILES TO BE PROCESSED
            /*
             * // show coverage stats in PhantomJS Console/CMD
             * 'build/js/*.js': ['coverage'],
             *
             * // process the html files with 'karma-ng-html2js-preprocessor' plugin
             * 'build/view/*.html': ['ng-html2js']
             */
            preprocessors: {
                'src/js/*.js': [ 'coverage' ],
                // 'src/view/**/*.html': ['ng-html2js']
            },

            // TWEAK THE PATH OF HTML TEMPLATE (IF ANY) TO MATCH THE PATH DEFINED IN THE DIRECTIVE's 'templateUrl' VALUE
            /*
             * // Get rid of '<pathA>/' in the template path configured in karma.conf.js, e.g. '<pathA>/view/ template.html'
             * stripPrefix:
             *
             * // Add '<pathB>' after the stripPrefix above (if any), e.g. '<pathB>/view/template.html'
             * // Make sure after preprendPrefix, the path must be same the one defined in templateUrl
             * // e.g. templateUrl: '<pathB>/view/template.html'
             * prependPrefix:
             */
            ngHtml2JsPreprocessor: {
                stripPrefix: 'dist/',
                prependPrefix: '../',
            },

            // REPORT COVERAGE
            reporters: [
                'spec',             // matches npm pkg "karma-spec-reporter"
                'coverage'          // matches npm pkg "karma-coverage"
            ],

            // LOG
            colors: true,
            // logLevel: config.LOG_INFO
        }
    }

}; // end module.exports

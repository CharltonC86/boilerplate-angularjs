## Main Folder Structure
    dist/                           // distribution (output folder) - where `gulp build` command will build to

    src/                            // development/source code
        fonts/
        img/                        // images

        sass/                       // global sass, incl. libraries
            _common/                // 3rd party, extendable, mixins etc
            main.scss               // main entry scss

        js/                         // global js, incl. libraries
            lib/                    // 3rd party libraries
            route/                  // routing
                route.js
            directive/              // custom attr (non-ui) that modifies the html tag or element behavior
                <name>.js
            service/
                data-store.js       // angular service for storing the ajax content
                data-call.js        // angular service for ajax - getting the content
            main.js                 // main app definition
            ng-html.js              // compiled angular template js from html, e.g. ui/view/home/template.html

        ui/                         // atomic ui module by scale in order (component < module < view)
            element/                // Lvl-1: Single element that can be styled class only, e.g. <button>, <h1>
                <elementName>
                    template.html   // html template
                    style.scss      // style related to the view/component/element
                    directive.js    // custom html tag that wraps the template (can be presentational or not)
            component/              // Lvl-2: Consists of >=1 components, e.g. <table>, <ul>
                <componentName>
                    template.html
                    style.scss
                    directive.js
            view/                   // Lvl-3: Consists of >=1 components and/or modules, which is used for routing
                <viewName>
                    style.scss
                    template.html
                    controller.js   // matching controller for the view template (routing only)

        index.html                  // Index page for Angular App (will be coped to "dist" folder)

    config.js                       // configuration for Gulp
    karma.conf.js                   // configuration for Angular Unit test
    .jshintrc                       // linting config for Javascript files
    gulpfile.js                     // Gulp Task Runner Script
    package.json                    // Specify NPM Dev Dependencies/Packages to be installed


## FED Getting started
1. Download & Install NodeJS

2. In CMD, Install Gulp globally:
`npm install -g gulp`

3. In CMD, Navigate to your project directory and then install all the packages specified in package.json:<br>
`npm install`

4. Run a server (Page will automatically opened in browser)
`gulp serv`


## Other available Gulp command
`gulp tasks`                // list all the tasks1   <br>
`gulp build`                // build all for development<br>
`gulp build --prod`         // build all for production<br>
`gulp build-css`            // build css for development<br>
`gulp build-css --prod`     // build css for production
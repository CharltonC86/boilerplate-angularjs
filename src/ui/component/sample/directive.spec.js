describe('Test', function(){
    beforeEach(function(){
        module('demo');
    });

    describe('Directive Test', function(){
        var scope,
            elem;

        beforeEach(inject(function($rootScope, $compile){
            var ngElem = angular.element('<sample ng-click="onClick"/>');
            scope = $rootScope.$new();
            elem = $compile(ngElem)(scope);
            scope.$digest();
        }));

        it('contains text', function(){
            expect(elem.html()).toContain('lorem');
        });

        it('click event', function(){
            elem.find('p').triggerHandler('click');
            scope.$digest();
            expect(scope.text).toBe(0);
        });
    });
});
(function(){
    'use strict';

    angular
        .module('demo')
        .directive('sample', sample);

    sample.$inject = [];
    function sample () {
        return {
            template: '<p ng-transclude>lorem</p>',
            transclude: true,
            replace: true,
            restrict: 'E',
            link: function(scope, iElem, iAttr){
                scope.text = 0;
                scope.onClick = function(){
                    console.log('click');
                    scope.text = 1;
                };
            }
        }
    }
})();


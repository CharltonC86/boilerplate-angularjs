describe('Test', function(){
    beforeEach(function(){
        module('demo');
    });

    describe('Controller Test', function(){
        var homeCtrlerScope,
            ctrler,
            mockedContentData,
            mockedDataStore;

        beforeEach(inject(function($rootScope, $controller){
            homeCtrlerScope = $rootScope.$new();
            mockedContentData = [];
            mockedDataStore = { home: null };
            ctrler = $controller('aboutCtrler', {
                $scope: homeCtrlerScope,
                contentData: mockedContentData,
                dataStore: mockedDataStore}
            );
        }));

        it('$scope.users to be empty', function(){
            expect(homeCtrlerScope.users).toEqual([]);
        });
    });
});
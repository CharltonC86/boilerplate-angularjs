(function(){
    'use strict';

    angular
        .module('demo')
        .controller('aboutCtrler', aboutCtrler);

    aboutCtrler.$inject = [ '$scope', 'contentData', 'dataStore' ];
    function aboutCtrler ($scope, contentData, dataStore) {
        // dataStore.about = contentData.concat();
        $scope.users = contentData.concat();
    }
})();


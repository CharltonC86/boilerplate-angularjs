 (function(){
    'use strict';

    angular
        .module('demo')
        .controller('homeCtrler', homeCtrler);

    homeCtrler.$inject = [ '$scope', 'contentData', 'dataStore' ]
    function homeCtrler ($scope, contentData, dataStore) {
        // dataStore.home = contentData.concat();
        $scope.users = contentData.concat();
    }
})();
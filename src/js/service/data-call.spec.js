describe('Test', function(){
    beforeEach(function(){
        module('demo');
    });

    describe('Ajax/Promise Test', function(){
        var defer,
            result;

        beforeEach(inject(function($q){
            defer = $q.defer();
        }));


        it('should resolve', inject(function($rootScope, dataCall){
            var mockedResult = [1,2,3];

            spyOn(dataCall, 'getPageContent').and.returnValue(defer.promise);
            dataCall.getPageContent().then(function(resolvedVal){
                result = resolvedVal;
            });
            defer.resolve(mockedResult);
            $rootScope.$apply();
            expect(result).toBe(mockedResult);
        }));

        it('should reject', inject(function($rootScope, dataCall){
            var mockedResult = 'error msg';

            spyOn(dataCall, 'getPageContent').and.returnValue(defer.promise);
            dataCall.getPageContent().catch(function(resolvedVal){
                result = resolvedVal;
            });
            defer.reject(mockedResult);
            $rootScope.$apply();
            expect(result).toBe(mockedResult);
        }));
    });

});
describe('Test', function(){
    beforeEach(function(){
        module('demo');
    });

    describe('Service Test', function(){
        it('dataStore', inject(function(dataStore){
            expect(dataStore.home).toBe(null);
            expect(dataStore.about).toBe(null);
        }));
    });

});
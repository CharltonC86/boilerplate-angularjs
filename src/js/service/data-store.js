(function(){
    'use strict';

    angular
        .module('demo')
        .factory('dataStore', dataStore);

    dataStore.$inject = []
    function dataStore() {
        return {
            home: null,
            about: null
        };
    }
})();

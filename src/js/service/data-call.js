(function(){
    'use strict';

    angular
        .module('demo')
        .factory('dataCall', dataCall);

    dataCall.$inject = ['$http', '$q'];
    function dataCall($http, $q) {
        // Dummy Json Call
        var baseUrl = 'https://jsonplaceholder.typicode.com/';
        return {
            getPageContent: function(pageName) {
                var d = $q.defer();
                $http({
                    url: baseUrl.concat(pageName === 'home' ? 'posts' : 'users'),
                    method: 'GET'
                }).success(function(resp) {
                    d.resolve(resp);
                }).error(function(resp) {
                    d.reject(false);
                });
                return d.promise;
            }
        };
    }

})();
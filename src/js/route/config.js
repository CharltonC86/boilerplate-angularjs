(function(){
    'use strict';

    angular
        .module('demo')
        .config(config);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
            .otherwise( { redirectTo: '/'} )
            .when('/', {
                templateUrl: 'view/home/template.html',
                controller: 'homeCtrler',
                resolve: {
                    contentData: ['dataCall', function(dataCall) {
                        return dataCall.getPageContent('home');
                    }]
                }
            })
            .when('/about', {
                templateUrl: 'view/about/template.html',
                controller: 'aboutCtrler',
                resolve: {
                    contentData: ['dataCall', function(dataCall) {
                        return dataCall.getPageContent('about');
                    }]
                }
            });
    }

})();
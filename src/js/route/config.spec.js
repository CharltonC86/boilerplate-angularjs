describe('Test', function(){
    beforeEach(function(){
        module('demo');
    });

    describe('Router Test', function(){
        it('router config', inject(function($route){
            expect($route.routes[null].redirectTo).toEqual('/');
            expect($route.routes['/'].controller).toBe('homeCtrler');
            expect($route.routes['/'].templateUrl).toBe('home/template.html');
            expect($route.routes['/about'].controller).toBe('aboutCtrler');
            expect($route.routes['/about'].templateUrl).toBe('about/template.html');
        }));

        it('home view', inject(function($httpBackend, $location, $rootScope, $route, $q, dataCall){
            var defer = $q.defer();
            spyOn(dataCall, 'getPageContent').and.returnValue(defer.promise);
            $httpBackend.expectGET('').respond(200);
            defer.resolve('');
            $location.path('/');
            $rootScope.$digest();

            expect($location.path()).toBe('/');
            expect($route.current.templateUrl).toBe('home/template.html');
            expect($route.current.controller).toBe('homeCtrler');

            $location.path('/about');
            $rootScope.$digest();
            expect($location.path()).toBe('/about');
            expect($route.current.templateUrl).toBe('about/template.html');
            expect($route.current.controller).toBe('aboutCtrler');
        }));
    });
});
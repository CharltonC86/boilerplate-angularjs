describe('Test', function(){
    beforeEach(function(){
        module('demo');
    });

    describe('Filter Test', function(){
        it('should convert to either uppercase or lowercase', inject(function(sampleFilter){
            expect(sampleFilter('abc', true)).toBe('ABC');
            expect(sampleFilter('ABC', false)).toBe('abc');
        }));
    });

});
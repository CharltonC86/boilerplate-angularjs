(function(){
    'use strict';

    angular
        .module('demo')
        .filter('sample', sample);

    sample.$inject = [];
    function sample() {
        return function(val, optionVal){
            return optionVal ? val.toUpperCase() : val.toLowerCase();
        };
    }

})();